<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/HTML; charset=utf-8">
<title>Untitled Document</title>
<style type="text/css">
h1{text-align:center}
.form-wrp {width: 600px;margin: 30px auto 0px;background: #ffffff;padding: 20px;}
#results span{padding: 20px;background: #ffffff;margin: 5px 0;text-align: center;box-shadow: inset 0px 0px 3px 0px rgba(0, 0, 0, 0.06);border: 1px solid #00b4cc;display: block;}
</style>
<script lang="javascript" src="https://s3.us-east-2.amazonaws.com/comorbidity/js-xlsx-master/dist/xlsx.full.min.js"></script>
<!-- <script lang="javascript" src="https://s3.us-east-2.amazonaws.com/comorbidity/comorbid_js_files/uhmc_mp_comorbidity2.js"></script>
<script lang="javascript" src="https://s3.us-east-2.amazonaws.com/comorbidity/comorbid_js_files/CoMorbRules.js"></script>
<script lang="javascript" src="https://s3.us-east-2.amazonaws.com/comorbidity/comorbid_js_files/util.js"></script>
<script lang="javascript" src="https://s3.us-east-2.amazonaws.com/comorbidity/comorbid_js_files/require.js"></script> -->

<script src="https://sdk.amazonaws.com/js/aws-sdk-2.19.0.min.js"></script>
<!-- <script src="test.js" type="text/javascript"></script> -->
<!-- <script src="uhmc_mp_comorbidity2.js" type="text/javascript"></script>  -->
<!-- <script src="CoMorbRuless.js" type="text/javascript"></script>  -->
</head>

<body>

<?php
$access_key         = "AKIAJESHBSYFHPR5P34A"; //User Access Key
$secret_key         = "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"; //secret key
$my_bucket          = "comorbidity"; //bucket name
$region				= "us-east-1"; //bucket region
$allowd_file_size	= "1048579, 10485760"; //This example allows a file size from 1 to 10 MiB

//dates
$short_date         = gmdate('Ymd'); //short date
$iso_date           = gmdate("Ymd\THis\Z"); //iso format date
$expiration_date    = gmdate('Y-m-d\TG:i:s\Z', strtotime('+1 hours')); //policy expiration 1 hour from now
$presigned_url_expiry    = 3600; //Presigned URL validity expiration time (3600 = 1 hour)

$policy = array(
'expiration' => gmdate('Y-m-d\TG:i:s\Z', strtotime('+6 hours')),
'conditions' => array(
	array('bucket' => $my_bucket),  
	array('acl' => 'public-read'),  
	array('starts-with', '$key', ''),  
	array('starts-with', '$Content-Type', ''),  
	array('success_action_status' => '201'),  
	array('x-amz-credential' => implode('/', array($access_key, $short_date, $region, 's3', 'aws4_request'))),  
	array('x-amz-algorithm' => 'AWS4-HMAC-SHA256'),  
	array('x-amz-date' => $iso_date),  
	array('x-amz-expires' => ''.$presigned_url_expiry.''),  
));

$policybase64 = base64_encode(json_encode($policy));	

$kDate = hash_hmac('sha256', $short_date, 'AWS4' . $secret_key, true);
$kRegion = hash_hmac('sha256', $region, $kDate, true);
$kService = hash_hmac('sha256', "s3", $kRegion, true);
$kSigning = hash_hmac('sha256', "aws4_request", $kService, true);
$signature = hash_hmac('sha256', $policybase64 , $kSigning);
?>
<h1>Comorbidity System</h1>
<div class="form-wrp">
<div id="results"><!-- server response here --></div>
<form action="http://<?=$my_bucket?>.s3-<?=$region?>.amazonaws.com" method="post" id="aws_upload_form"  enctype="multipart/form-data">
<input type="hidden" name="acl" value="public-read">
<input type="hidden" name="success_action_status" value="201">
<input type="hidden" name="policy" value="<?=$policybase64?>">
<input type="hidden" name="X-amz-credential" value="<?=$access_key?>/<?=$short_date?>/<?=$region?>/s3/aws4_request">
<input type="hidden" name="X-amz-algorithm" value="AWS4-HMAC-SHA256">
<input type="hidden" name="X-amz-date" value="<?=$iso_date?>">
<input type="hidden" name="X-amz-expires" value="<?=$presigned_url_expiry?>">
<input type="hidden" name="X-amz-signature" value="<?=$signature?>">
<input type="hidden" name="key" value="">
<input type="hidden" name="Content-Type" value="">
<input type="button" value="Download Template" onclick="window.location.href='https://s3.us-east-2.amazonaws.com/comorbidity/Template.xlsx'" />
<input type="file" name="file" value="Upload" />
<input type="submit" value="Submit" />

<!-- <input type="file" name="file" />
<input type="submit" value="Upload File" />
<a href = "https://s3.us-east-2.amazonaws.com/comorbidity/Template.xlsx" download> Download Template </a>
 -->

</form>
</div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>

$("#aws_upload_form").submit(function(e) {
    e.preventDefault();
	the_file = this.elements['file'].files[0];
	var filename = "Uploaded_Template"+ '.' + the_file.name.split('.').pop();//Date.now()
	$(this).find("input[name=key]").val(filename);
	$(this).find("input[name=Content-Type]").val(the_file.type);
	

    var post_url = $(this).attr("action"); //get form action url
    var form_data = new FormData(this); //Creates new FormData object
    $.ajax({
        url : post_url,
        type: 'post',
		datatype: 'xml',
        data : form_data,
		contentType: false,
        processData:false,
		xhr: function(){
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload){
				var progressbar = $("<div>", { style: "background:#FFFFFF;height:100px;margin:100px 0;" }).appendTo("#results"); //create progressbar
				xhr.upload.addEventListener('progress', function(event){
						var percent = 0;
						var position = event.loaded || event.position;
						var total = event.total;
						if (event.lengthComputable) {
							percent = Math.ceil(position / total * 100);
							progressbar.css("width", + percent +"%");
						}
				}, true);
			}
			return xhr;
		}
    }).done(function(response){
		var url = $(response).find("Location").text(); //get file location
		var the_file_name = $(response).find("Key").text(); //get uploaded file name
        // $("#results").html("<span>File has been uploaded, Here's your file <a href=" + url + ">" + the_file_name + "</a></span>"); //response
        //uploaded_url=url;
    function upload_to_s3(jsonobj)
    {

        
            AWS.config.update({
                "accessKeyId": "AKIAJGWITUEUOE6U525Q",
                "secretAccessKey": "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",
                "region": "us-east-2"
            });
            var s3 = new AWS.S3();
            var params = {
                Bucket: 'comorbidity',
                Key: 'jsonobj.txt',
                Body: JSON.stringify(jsonobj),
                ContentType: "text/plain",
                ACL: 'public-read'
            }; 
            s3.putObject(params,function(err,data){console.log(JSON.stringify(err)+" "+JSON.stringify(data));});
        
    }




                                                    var uploaded_url=url;
                                                    function filePicked1() {

//                                                  file = this.files[0];

//                                                  if (file.type != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
//                                                  {
//                                                      alert("invalid file type")
//                                                      return
//                                                  }
                                                
                                                    var url1 = uploaded_url;
                                                        var oReq = new XMLHttpRequest();
                                                        oReq.open("GET", url1, true);
                                                        oReq.responseType = "arraybuffer";

                                                    oReq.onload = function(e)
 {
                                                       // alert("there   on load "+e)
                                                          var arraybuffer = oReq.response;

                                                          /* convert data to binary string */
                                                          var data = new Uint8Array(arraybuffer);
                                                          var arr = new Array();
                                                          for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                                                          var bstr = arr.join("");

                                                          /* Call XLSX */
                                                          var workbook = XLSX.read(bstr, {type:"binary"});

                                                          /* DO SOMETHING WITH workbook HERE */
                                                          var sheet_1 = workbook.SheetNames[0];
                                                          var sheet_2 = workbook.SheetNames[1];
                                                          var sheet_3 = workbook.SheetNames[2];
                                                          var sheet_4 = workbook.SheetNames[3];
                                                          /* Get worksheet */
                                                          var worksheet1 = workbook.Sheets[sheet_1];
                                                          var worksheet2 = workbook.Sheets[sheet_2];
                                                          var worksheet3 = workbook.Sheets[sheet_3];
                                                          var worksheet4 = workbook.Sheets[sheet_4];
                                                          var tempVar2 = (XLSX.utils.sheet_to_json(worksheet2));
                                                          var tempVar3 = (XLSX.utils.sheet_to_json(worksheet3));
                                                          var tempVar4 = (XLSX.utils.sheet_to_json(worksheet4));
                                                          var tempVar = XLSX.utils.sheet_to_json(worksheet1,{defval:""});
                                                          var jsonObj = {"TEMP" : tempVar[0],"TEMP1" : tempVar2[0],"TEMP2" : tempVar3[0],"TEMP3" : tempVar4[0]};
                                                          // alert(JSON.stringify(jsonObj));
                                                         // alert(JSON.stringify(jsonObj))

upload_to_s3(jsonObj);

//setTimeout(function(){}, 1000); 
                                                   // window.location.href ="http://localhost:3000/comorb"//"http://ec2-54-173-142-211.compute-1.amazonaws.com:3000/comorb";
                                                    window.location.href = "http://ec2-54-173-142-211.compute-1.amazonaws.com:3000/comorb";


//window.location.reload();
                                                    //      init(jsonObj);
           // make_file("dhfbs");

                                                          // var fs = require('fs');

                                                          // fs.writeFile('helloworld.txt', 'Hello World!', function (err) {
                                                          //     if (err) 
                                                          //         return console.log(err);
                                                          //     console.log('Wrote Hello World in file helloworld.txt, just check it');
                                                          // });                                                          
                                                          //alert("parsed")
                                                         }

                                                      oReq.send(); 
                                                    
                                                        }


filePicked1()

        }).error(function() {
      console.log( arguments);
    });
});

</script>
</body>
</html>

