var express = require('express');
var app = express();
function make_file(data)
{

  var fs = require('fs');

  fs.writeFile('helloworld.txt', 'Hello World!', function (err) {
      if (err) 
          return console.log(err);
      console.log('Wrote Hello World in file helloworld.txt, just check it');
  }); 
}

var execPHP = require('./myphpinterface.js')();

execPHP.phpFolder = '.';

app.use('*.php',function(request,response,next) {
	console.log(request.originalUrl)
	execPHP.parseFile(request.originalUrl,function(phpResult) {
		console.log(phpResult);
		response.write(phpResult);
		response.end();
	});
});

app.listen(8080, function () {
	console.log('Node server listening on port 8080!');
});
